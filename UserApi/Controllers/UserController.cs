﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserApi.Models;
using UserApi.Repositry;

namespace orderApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(IUserRepository repository)
        {
            _dataRepository = repository;
        }
        private readonly IUserRepository _dataRepository;

        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(long id)
        {
            Users user = _dataRepository.Get(id);

            if (user == null)
            {
                return NotFound("The Employee record couldn't be found.");
            }

            return Ok(user);
        }
    }
}