﻿using Microsoft.EntityFrameworkCore;
using UserApi.Models;

namespace UserApi
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>().HasData(
                new Users() { Id = 1, Name = "John", Email = "Developer", Age = 50 },
                new Users() { Id = 2, Name = "Chris", Email = "Manager", Age = 40 },
                new Users() { Id = 3, Name = "Mukesh", Email = "Consultant", Age = 20});
        }



    }
}



