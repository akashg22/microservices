﻿using UserApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApi.Repositry
{
    public class UserRepository : IUserRepository
    {

        readonly UserContext _userContext;

        public UserRepository(UserContext context)
        {
            _userContext = context;
        }
        public Users Get(long id)
        {
          return _userContext.Users.FirstOrDefault(p => p.Id == id);
        }
    }
}
