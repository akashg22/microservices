﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApi.Models;

namespace UserApi.Repositry
{
    public interface IUserRepository
    {
        Users Get(long id);
    }
}
