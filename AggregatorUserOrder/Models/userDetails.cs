﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AggregatorUserOrder.Models
{
    public class userDetails
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public double Age { get; set; }
    }

    public class orderDetails
    {
        public int orderId { get; set; }

        public double orderAmount { get; set; }


        public string orderDate { get; set; }
    }

    public class UserOrderDetails
    {
        public userDetails UserDetails { get; set; }
        public orderDetails[] orders { get; set; }

        

    }

}
