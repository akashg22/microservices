﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AggregatorUserOrder.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Steeltoe.Common.Discovery;

namespace AggregatorUserOrder.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserOrderController : ControllerBase
    {
        DiscoveryHttpClientHandler clientHandler;
        public UserOrderController(IDiscoveryClient client)
        {
            clientHandler = new DiscoveryHttpClientHandler(client);
        }

        
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(long id)
        {
            var client = new HttpClient(clientHandler, false);
            try
            {
                var UserOrderDetails = new UserOrderDetails();
                var userResult = await client.GetAsync("http://userApi/api/User/" + id);
                UserOrderDetails.UserDetails = JsonConvert.DeserializeObject<userDetails>(await userResult.Content.ReadAsStringAsync());
                var orderresult = await client.GetAsync("http://orderApi/api/order/" + id);
                orderDetails[] details = JsonConvert.DeserializeObject<orderDetails[]>(await orderresult.Content.ReadAsStringAsync());
                UserOrderDetails.orders = details;
                return Ok(UserOrderDetails);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
    }
}