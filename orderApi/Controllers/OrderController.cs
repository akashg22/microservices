﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using orderService.Models;

namespace orderService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        [Route("{id:int}")]
        [HttpGet]
        public IEnumerable<Order> Get(int id)
        {
            return getList().Where(p => p.customerId == id);
        }



        public List<Order> getList()
        {
            var orderList = new List<Order>() { new Order() { customerId=1,orderAmount=55,orderDate="25-05-2020",orderId =1},
            new Order() { customerId=1,orderAmount=55,orderDate="25-05-2020",orderId =2},
            new Order() { customerId=2,orderAmount=55,orderDate="25-05-2020",orderId =3},
            new Order() { customerId=3,orderAmount=55,orderDate="25-05-2020",orderId =4},
            new Order() { customerId=1,orderAmount=55,orderDate="25-05-2020",orderId =5},
            new Order() { customerId=1,orderAmount=55,orderDate="25-05-2020",orderId =6}};
            return orderList;

        }
    }
}