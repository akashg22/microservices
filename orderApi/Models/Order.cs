﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orderService.Models
{
    public class Order
    {
        public int orderId { get; set; }

        public double orderAmount { get; set; }


        public string orderDate { get; set; }



        public int customerId { get; set; }
    }
}
